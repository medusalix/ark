{
    "KPlugin": {
        "MimeTypes": [
            "@SUPPORTED_ARK_MIMETYPES_JSON@"
        ],
        "Name": "Ark Extract Here",
        "Name[ar]": "أرك استخرج هنا",
        "Name[az]": "Buraya çıxarın",
        "Name[bg]": "Извличане с Ark тук",
        "Name[ca@valencia]": "Extracció d'Ark ací",
        "Name[ca]": "Extracció de l'Ark aquí",
        "Name[cs]": "Rozbalit Arkem sem",
        "Name[el]": "Ark Εξαγωγή Εδώ",
        "Name[eo]": "Ark-Extrakti ĉi tie",
        "Name[es]": "Ark - Extraer aquí",
        "Name[eu]": "Ark Erauzi hemen",
        "Name[fi]": "Ark: pura tähän",
        "Name[fr]": "Extraire ici avec Ark",
        "Name[gl]": "Extraer aquí con Ark",
        "Name[he]": "חילוץ עם Ark לכאן",
        "Name[ia]": "Ark Extrahe Hic",
        "Name[it]": "Ark estrai qui",
        "Name[ka]": "Ark აქ გაშლა",
        "Name[ko]": "Ark 여기에 압축 풀기",
        "Name[nl]": "Ark: hier uitpakken",
        "Name[nn]": "Pakk ut med Ark her",
        "Name[pl]": "Wypakuj tutaj",
        "Name[pt]": "Ark - Extrair Aqui",
        "Name[pt_BR]": "Extrair com o Ark aqui",
        "Name[ru]": "Распаковать в эту папку",
        "Name[sl]": "Arkov ekstrakt tukaj",
        "Name[sv]": "Ark packa upp här",
        "Name[tr]": "Ark Buraya Çıkar",
        "Name[uk]": "Розпакувати сюди за допомогою Ark",
        "Name[x-test]": "xxArk Extract Herexx",
        "Name[zh_CN]": "Ark 解压到此位置",
        "Name[zh_TW]": "Ark 在此解壓縮"
    }
}
